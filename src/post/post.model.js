import mongoose from "mongoose"

module.exports = mongoose.model("Post", mongoose.Schema({
  title: String,
  body: String,
  date: {
    type: Date, default: Date.now
  },
  tags: [String]
}))
