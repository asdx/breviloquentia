import repository from "./post.repository"

let ROOT_API_PATH = "/posts"

let registerResource = (app) => {
  app.get(`${ROOT_API_PATH}`, (req, res) => repository.all(posts => res.json(posts)))
  app.get(`${ROOT_API_PATH}/:id`, (req, res) => repository.one(req.params.id, post => res.json(post)))
  app.post(`${ROOT_API_PATH}`, (req, res) => repository.save(req.body))
  app.put(`${ROOT_API_PATH}/:id`, (req, res) => repository.update(req.params.id, req.body))
  app.delete(`${ROOT_API_PATH}/:id`, (req, res) => repository.delete(id))
}

module.exports = { register: registerResource }
