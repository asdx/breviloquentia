import mockData from "~/post/post.mock.data"

module.exports = {
  one: (id, cb) => { cb(mockData[0]) },
  all: (cb) => { cb(mockData) },
  save: (post, cb) => { console.log(`saving ${post}`) },
  update: (id, post, cb) => { console.log(`updating ${id} with ${post}`) },
  delete: (id, cb) => { console.log(`deleting ${id}`) }
}
