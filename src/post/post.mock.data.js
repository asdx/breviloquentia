module.exports = [{
  id: "0",
  title: "Example post A",
  body: "Stuff goes here.",
  date: new Date(),
  tags: ["tagA", "tagB"]
},
{
  id: "1",
  title: "Example post B",
  body: "Interesting stuff goes here.",
  date: new Date(),
  tags: ["tagA", "tagC"]
},
{
  id: "2",
  title: "Example post C",
  body: "Hello.",
  date: new Date(),
  tags: ["tagD", "tagE"]
}]
