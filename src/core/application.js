import express from "express"
import bodyParser from "body-parser"
import cors from "cors"

import postResource from "~/post/post.resource"

let app = express()

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

postResource.register(app)

app.listen(process.env.PORT || 3210, () => console.log("server is up"))
